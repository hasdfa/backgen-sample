package main

import (
	"log"
	"flag"
	"backgen-sample/services/http/authService"

	"bgen/pkg/configUtils"
	"backgen-sample/bgenerated/controllers/ws"
	"backgen-sample/bgenerated/controllers/http"
	"backgen-sample/services/ws/msgService"
	"backgen-sample/bgenerated/controllers/context"
)


var (
	closing = make(chan error)
	configFile = flag.String("c", "configs/local.json", "Configuration file path")
)

func main() {
	flag.Parse()
	config, err := configUtils.ParseFile(configFile)
	if err != nil {
		log.Fatal(err)
	}


	wsServer := ws.NewServer()
	go RunWsServer(config.Ws, wsServer)

	httpServer := http.NewServer()
	go RunHttpServer(config.Http, httpServer)

	log.Fatal(<- closing)
}

func RunHttpServer(c *configUtils.HttpConfig, server *http.Server)  {
	server.Services = &context.HttpServicesSource {
		AuthService: authService.CreateAuthService(),
	}
	server.Init()

	for _, p := range c.Ports {
		go func(port *configUtils.ServerPort) {
			closing <- server.Start(port)
		}(p)
	}
}

func RunWsServer(c *configUtils.WsConfig, server *ws.Server) {
	server.Services = &ws.ServicesSource {
		Msg: &ws.MsgServiceSource{
			Company: msgService.CreateMsgCompanyService(),
		},
	}

	go func() {
		closing <- server.Start(c)
	}()
}