package context

import (
	"backgen-sample/bgenerated/controllers/ws/msg/models"
	"bgen/pkg/wsUtils"
)

type WSContext struct {
	*wsUtils.BaseContext
	Controllers *Controllers
}

func InitDefaultWSContext() WSContext {
	return WSContext {
		Controllers: &Controllers{
			Msg: &msgService{},
		},
	}
}

func (obj WSContext) Of(conn *wsUtils.Connection) *WSContext {
	c := obj
	c.Connection = conn

	return &c
}

func (obj *WSContext) Query(resolver wsUtils.QueryResolver) *WSContext {
	obj.Resolver = resolver
	return obj
}

type Controllers struct {
	Msg *msgService
}

// `/msg`
type msgService struct {
	Company *msgCompanyService
}

// `/msg/company`
type msgCompanyService struct {}

func (*msgCompanyService) Send(ctx *WSContext, r wsUtils.QueryResolver, m *models.ReceivedMessage) error {

	return nil
}