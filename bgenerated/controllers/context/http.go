package context

import "backgen-sample/bgenerated/controllers/http/authController"

type HttpServicesSource struct {
	AuthService     authController.IAuthService
	PasswordService authController.IPasswordService
}