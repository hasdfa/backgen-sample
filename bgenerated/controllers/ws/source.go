package ws

import (
	msgHandlers "backgen-sample/bgenerated/controllers/ws/msg/handlers"
)

type ServicesSource struct {
	Msg *MsgServiceSource
}

type MsgServiceSource struct {
	Company msgHandlers.CompanyService
}