package ws

import (
	"backgen-sample/bgenerated/controllers/context"
	"bgen/pkg/wsUtils"
	"strings"
	"backgen-sample/bgenerated/controllers/ws/msg/models"
)

func (s *Server) Route(context *context.WSContext, packet *wsUtils.Packet) {
	packet.Path = strings.TrimSuffix(packet.Path, "/")

	switch packet.Path {
	case "/msg/company":
		context.HandleError(s.Services.Msg.Company.Receive(context,
			models.ReceivedMessage{}.Unpack(packet.Content),
		))
	}
}