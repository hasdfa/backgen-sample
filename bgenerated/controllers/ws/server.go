package ws

import (
	"bgen/pkg/configUtils"
	"bgen/pkg/wsUtils"
	"backgen-sample/bgenerated/controllers/context"
)

type Server struct {
	Services    *ServicesSource

	parent      *wsUtils.Server
	context     context.WSContext
}

func NewServer() *Server {
	s := &Server {
		context: context.InitDefaultWSContext(),
	}

	s.parent = wsUtils.NewServer(s.reader)
	s.parent.Authorization = s.auth
	return s
}

func (s *Server) Start(config *configUtils.WsConfig) (err error) {
	return s.parent.Start(config)
}

func (s *Server) reader(conn *wsUtils.Connection, p wsUtils.Packet){
	s.Route(s.context.Of(conn), &p)
}

func (s *Server) auth(conn *wsUtils.Connection) (string, error) {
	panic("implement me")
}

func (s *Server) LocalAddr() string {
	return s.parent.LocalAddr()
}

func (s *Server) Stop() error {
	return s.parent.Stop()
}