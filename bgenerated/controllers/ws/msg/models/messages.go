package models

import "bgen/pkg/wsUtils"

type Message struct {
	ChatUID string
	Content MessageContent
}

func (obj Message) Unpack(packet wsUtils.PackedData) *Message {
	obj.ChatUID = packet.String("chatUid")
	obj.Content.Unpack(packet.PackedData("content"))

	return &obj
}

type ReceivedMessage struct {
	*Message
	SenderUID string
}

func (obj ReceivedMessage) Unpack(packet wsUtils.PackedData) *ReceivedMessage {
	obj.Content.Unpack(packet)
	obj.ChatUID = packet.String("senderUid")

	return &obj
}

type MessageContent struct {
	Type    int
	Content map[string]interface{}
}

func (obj MessageContent) Unpack(packet wsUtils.PackedData) *MessageContent {
	obj.Content = packet["content"].(map[string]interface{})
	obj.Type = packet.Int("type")

	return &obj
}
