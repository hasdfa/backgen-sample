package handlers

import (
	"backgen-sample/bgenerated/controllers/ws/msg/models"
	"backgen-sample/bgenerated/controllers/context"
)

type CompanyService interface {
	Receive(context *context.WSContext, request *models.ReceivedMessage) error
}