package http

import (
	"github.com/kataras/muxie"
	"net/http"
	"fmt"
	"sync"
	"errors"
	"strings"
	"strconv"
	"backgen-sample/services/additions/rolesService"
	"backgen-sample/bgenerated/additions/roles"
	"backgen-sample/bgenerated/controllers/http/authController"
	"bgen/pkg/configUtils"
	"backgen-sample/bgenerated/controllers/http/middlewares"
	"backgen-sample/bgenerated/controllers/context"
)

type Server struct {
	Services *context.HttpServicesSource

	mux      *muxie.Mux
	servers  []*http.Server

	wg     *sync.WaitGroup

	mask   string
}

func NewServer() *Server {
	return &Server{
		wg: &sync.WaitGroup{},
		mask: `{ "server": "http:", "path": "%s" }`,
	}
}

func (s *Server) Init() {
	s.mux = muxie.NewMux()
	s.mux.PathCorrection = true
	s.mux.Use(s.Logger)

	// groups
	authGroup := s.mux.Of("/auth")
	groupGroup := s.mux.Of("/group")

	// init controllers
	authController.Bind(authGroup, s.Services.AuthService, s.Services.PasswordService)

	// init middlewares
	groupGroup.Use(middlewares.AuthMiddleware)

	// addition: roles
	controller := roles.NewRolesAdditionController(rolesService.CreateRolesService())

	groupGroup.Use(controller.CheckRoleMiddleware("group.market.1"))
}

func (s *Server) Start(port *configUtils.ServerPort) error {
	s.wg.Add(1)
	server := &http.Server{
		Addr: fmt.Sprintf(":%d", port.Value),
		Handler: s.mux,
	}

	if port.Tls != nil {
		fmt.Println("Starterd http server with tls at " + server.Addr)
		return server.ListenAndServeTLS(port.Tls.CertFile, port.Tls.KeyFile)
	}

	fmt.Println("Starterd http server at " + server.Addr)
	return server.ListenAndServe()
}

func (s *Server) Wait() {
	s.wg.Wait()
}

func (s *Server) Logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.URL.Port())
		next.ServeHTTP(w, r)
	})
}

func (s *Server) Stop(addr uint16) error {
	for _, srv := range s.servers {
		if strings.HasSuffix(srv.Addr, ":" + strconv.Itoa(int(addr))) {
			return srv.Close()
		}
	}
	return errors.New("unknown server port")
}

func (s *Server) StopAll() (err error) {
	for _, srv := range s.servers {
		if serr := srv.Close(); serr != nil {
			err = serr
		}
	}
	return err
}