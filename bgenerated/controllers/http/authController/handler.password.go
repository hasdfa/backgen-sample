package authController

import (
	"github.com/kataras/muxie"
	"net/http"
	"bgen/pkg/httpUtils"
	"bgen/pkg/serviceUtils"
)

type IPasswordService interface {
	Reset(*LoginRequest) (*LoginResponse, serviceUtils.Error)
	ResetApply(*LoginRequest) (*LoginResponse, serviceUtils.Error)
}

type IPasswordHandler interface {
	Bind(muxie.SubMux)
}

type passwordGroup struct {
	service IPasswordService
}

func bindPassword(mux muxie.SubMux, service IPasswordService) {
	obj := passwordGroup{ service: service }

	mux.HandleFunc("/reset", obj.reset)
	mux.HandleFunc("/reset/apply", obj.resetApply)
}

func (obj *passwordGroup) reset(w http.ResponseWriter, r *http.Request) {
	if r.Method != httpUtils.POST {
		httpUtils.HttpError(w,
			httpUtils.MIMETextPlainCharsetUTF8,
			"Unsupported request method: " + r.Method,
			http.StatusMethodNotAllowed,
		)
		return
	}

	contentType := r.Header.Get(httpUtils.HeaderContentType)
	if contentType == "" {
		contentType = httpUtils.MIMEApplicationJSONCharsetUTF8
	}

	var request = new(LoginRequest)
	err := httpUtils.UnmarshalRequest(r, request)

	if err != nil {
		httpUtils.BadRequestError(w, contentType, err.Error())
		return
	}

	response, serr := obj.service.Reset(request)
	if err != nil {
		httpUtils.HttpServiceError(w, contentType, serr)
		return
	}

	bin, contentType := httpUtils.MarshalContent(response, contentType)
	if err == nil {
		httpUtils.InternalServerError(w, contentType, err.Error())
	}

	w.Write(bin)
}

func (obj *passwordGroup) resetApply(w http.ResponseWriter, r *http.Request) {
	if r.Method != httpUtils.POST {
		httpUtils.BadRequestError(w, httpUtils.MIMETextPlainCharsetUTF8, "Unsupported request method: " + r.Method)
		return
	}

	contentType := r.Header.Get(httpUtils.HeaderContentType)
	if contentType == "" {
		contentType = httpUtils.MIMEApplicationJSONCharsetUTF8
	}

	var request = new(LoginRequest)
	err := httpUtils.UnmarshalRequest(r, request)

	if err != nil {
		httpUtils.BadRequestError(w, contentType, err.Error())
		return
	}

	response, serr := obj.service.ResetApply(request)
	if err != nil {
		httpUtils.HttpServiceError(w, contentType, serr)
		return
	}

	bin, contentType := httpUtils.MarshalContent(response, contentType)
	if err == nil {
		httpUtils.InternalServerError(w, contentType, err.Error())
	}

	w.Write(bin)
}