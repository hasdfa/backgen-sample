package authController

import (
	"net/http"
	"bgen/pkg/serviceUtils"
	"github.com/kataras/muxie"
	"bgen/pkg/httpUtils"
)

type IAuthService interface {
	Login(*LoginRequest) (*LoginResponse, serviceUtils.Error)
	Register(interface{}) (interface{}, serviceUtils.Error)
}

type IAuthHandler interface {
	Bind(muxie.SubMux)
}

type authGroup struct {
	service IAuthService
}

func Bind(mux muxie.SubMux, s1 IAuthService, s2 IPasswordService) {
	obj := authGroup{ service: s1 }

	mux.HandleFunc("/login", obj.login)
	mux.HandleFunc("/register", obj.register)

	bindPassword(mux.Of("/password"), s2)
}

func (obj *authGroup) login(w http.ResponseWriter, r *http.Request) {
	contentType := r.Header.Get(httpUtils.HeaderContentType)
	if contentType == "" {
		contentType = httpUtils.MIMETextPlain
	}

	if r.Method != httpUtils.POST {
		httpUtils.BadRequestError(w, contentType, "Unsupported request method: " + r.Method)
		return
	}

	var request = new(LoginRequest)
	err := httpUtils.UnmarshalRequest(r, request)

	if err != nil {
		httpUtils.BadRequestError(w, contentType, err.Error())
		return
	}

	response, serr := obj.service.Login(request)
	if serr != nil {
		httpUtils.HttpServiceError(w, contentType, serr)
		return
	}

	bin, contentType := httpUtils.MarshalContent(response, contentType)

	w.Header().Set("Content-Type", contentType)
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(http.StatusOK)
	w.Write(bin)
}

func (obj *authGroup) register(w http.ResponseWriter, r *http.Request) {
	contentType := r.Header.Get(httpUtils.HeaderContentType)
	if contentType == "" {
		contentType = httpUtils.MIMETextPlain
	}

	if r.Method != httpUtils.POST {
		httpUtils.BadRequestError(w, contentType, "Unsupported request method: " + r.Method)
		return
	}

	var request = new(LoginRequest)
	err := httpUtils.UnmarshalRequest(r, request)

	if err != nil {
		httpUtils.BadRequestError(w, contentType, err.Error())
		return
	}

	response, serr := obj.service.Register(request)
	if serr != nil {
		httpUtils.HttpServiceError(w, contentType, serr)
		return
	}
	bin, contentType := httpUtils.MarshalContent(response, contentType)

	w.Header().Set("Content-Type", contentType)
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(http.StatusOK)
	w.Write(bin)
	w.Write(bin)
}