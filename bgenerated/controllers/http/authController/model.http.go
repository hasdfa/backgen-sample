package authController

import (
	"net/http"
)

type LoginRequest struct {
	Email    string `validator:"required,email"`
	Password string `validator:"required,lte=20,gte=6"`
	Device   Device
}

func (obj *LoginRequest) UnmarshalGET(contentType string, r *http.Request) (err error) {
	if err = r.ParseForm(); err != nil {
		return err
	}

	obj.Email = r.Form.Get("email")
	obj.Password = r.Header.Get("api-token")

	return err
}

type LoginResponse struct {
	UserToken *UserToken
}