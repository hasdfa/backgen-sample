package authController

type UserToken struct {
	Hash         string
	RefreshToken *RefreshToken
}

type RefreshToken struct {
	Hash string
}

type Device struct {
	Fingerprint string
	Location    *Location
}

type Location struct {
	Latitude  float64
	Longitude float64
}