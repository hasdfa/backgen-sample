package roles

type (
	rootRole        string
	childNode string

	groupRootRole   string
	chatRootRole    string
	marketRootRole  string
	tendersRootRole string
)

//noinspection GoUnusedGlobalVariable
var (
	AllRoles = map[string]interface{} {
		"group": map[string]interface{} {
			"info": []int { 1, 2, 3, 4, 5 },
			"partners": []int { 1, 2, 3, 4, 5 },
			"roles": []int { 1, 2, 3, 4, 5 },
		},
	}
)

func (r groupRootRole) Info() childNode {
	return childNode(r + ".info")
}

func (c childNode) Read() string {
	return string(c) + ".1"
}

func (c childNode) Write() string {
	return string(c) + ".2"
}

func (c childNode) AllowRead() string {
	return string(c) + ".3"
}

func (c childNode) AllowWrite() string {
	return string(c) + ".4"
}

func (c childNode) RootAccess() string {
	return string(c) + ".5"
}