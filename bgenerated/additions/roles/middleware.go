package roles

import (
	"net/http"
	"github.com/kataras/muxie"
	"bgen/pkg/httpUtils"
)

type IRolesAdditionService interface {
	CheckRole(r *http.Request, role string) bool
	ErrorMessage(role string) interface{}
}

type IRolesAdditionController interface {
	CheckRoleMiddleware(role string) muxie.Wrapper
}

type rolesAdditionController struct {
	service IRolesAdditionService
}

func (obj *rolesAdditionController) CheckRoleMiddleware(role string) muxie.Wrapper  {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if !obj.service.CheckRole(r, role) {
				ct := r.Header.Get(httpUtils.HeaderContentType)
				if ct == "" {
					ct = httpUtils.MIMEApplicationJSONCharsetUTF8
				}

				httpUtils.ForbiddenError(w, ct, http.StatusText(http.StatusForbidden))
			}

			next.ServeHTTP(w, r)
		})
	}
}

func NewRolesAdditionController(service IRolesAdditionService) IRolesAdditionController {
	return &rolesAdditionController{ service }
}
