package ws

import (
	"net/http"
	"runtime"
	"bgen/pkg/httpUtils"
	"bgen/pkg/wsUtils"
)

var (
	HandshakeHeader = http.Header{
		"X-Go-Version": []string{ runtime.Version() },
	}
)

func Authorize(conn *wsUtils.Connection) (string, error) {
	conn.Write(wsUtils.Packet{
		Path: "/auth",
	})

	if string(key) == httpUtils.HeaderAuthorization {
		panic("implement me")

		/*
		token := string(value)

		user, err := db.FindByToken(token)
		if err == nil {
		    return user.Id, nil
		}

		return "", ws.RejectConnectionError(
			ws.RejectionReason(err.Error()),
			ws.RejectionStatus(http.StatusUnauthorized),
		)
		*/
	}

	return "", nil
}