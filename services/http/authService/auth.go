package authService

import (
	"backgen-sample/bgenerated/controllers/http/authController"
	"bgen/pkg/serviceUtils"
	"encoding/base64"
	"crypto/sha512"
	"github.com/pborman/uuid"
	"net/http"
)

/* DO NOT EDIT */
type authService struct {}

func CreateAuthService() authController.IAuthService {
	return &authService{}
}
/* END */

func (obj *authService) Login(request *authController.LoginRequest) (*authController.LoginResponse, serviceUtils.Error) {
	return &authController.LoginResponse{
		UserToken: &authController.UserToken{
			Hash: base64.StdEncoding.EncodeToString(sha512.New().Sum([]byte(uuid.New()))),
			RefreshToken: &authController.RefreshToken{
				Hash: base64.StdEncoding.EncodeToString(sha512.New().Sum([]byte(uuid.New()))),
			},
		},
	}, nil
}

func (obj *authService) Register(request interface{}) (interface{}, serviceUtils.Error) {
	return nil, serviceUtils.NewServiceErrorCode(http.StatusNotImplemented)
}