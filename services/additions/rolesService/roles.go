package rolesService

import (
	"backgen-sample/bgenerated/additions/roles"
	"net/http"
)

type rolesService struct {

}

func CreateRolesService() roles.IRolesAdditionService {
	return &rolesService{}
}

func (obj *rolesService) CheckRole(r *http.Request, role string) bool {
	/* Sample:

	authToken := r.Header.Get(httpUtils.HeaderAuthorization)

	userRole := db.GetUserByToken(authToken).GetRole()
	return userRole == role
	*/

	panic("implement me")
}

func (obj *rolesService) ErrorMessage(role string) interface{} {
	return map[string]interface{}{
		"message": "You have no access to this path",
	}
}