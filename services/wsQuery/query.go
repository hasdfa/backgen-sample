package wsQuery

import (
	"bgen/pkg/wsUtils"
)

const (
	uidsKey = "user-ids"
	groupIdsKey = "group-ids"
)

type usersQuery struct {
	base  wsUtils.Query
}

func New() *usersQuery {
	return &usersQuery{
		base: wsUtils.NewQuery(),
	}
}

func (obj *usersQuery) Users(uids ...string) *usersQuery {
	obj.base.Set(uidsKey, uids)
	return obj
}

func (obj *usersQuery) Groups(uids ...string) *usersQuery {
	obj.base.Set(groupIdsKey, uids)
	return obj
}

func (obj *usersQuery) Build() wsUtils.QueryResolver {
	return &resolverImpl{ obj.base }
}