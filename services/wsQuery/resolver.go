package wsQuery

import (
	"bgen/pkg/wsUtils"
)

type resolverImpl struct {
	query wsUtils.Query
}

func (obj *resolverImpl) Connections() []string {
	uids, _ := obj.query.GetStringArray(uidsKey)

	groupUids, _ := obj.query.GetStringArray(groupIdsKey)
	//
	// gruids := db.GetGroupsUsers(groupUids)
	// uids = append(uids, gruids...)
	//

	panic("implement me")

	return append(uids, groupUids...)
}
